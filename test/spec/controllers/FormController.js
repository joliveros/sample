(function() {
	'use strict';

	var root = this;

	root.define([
		'controllers/FormController'
		, 'models/PatientForm'
		, 'test_user'
		, 'reqres'
		],
		function( 
			FormController
			, TestForm
			, TestUser
			, reqres
			 ) {

			describe('FormController Controller', function () {
				before(function(done){
					//create test user
					TestUser.LoginTestStaffUser(done)				
				})
				after(function(done){
					//delete test user
					TestUser.Delete(done)
				})
				it('should be an instance of FormController Controller', function () {
					var formController = new FormController();
					expect(formController).to.be.an.instanceof( FormController );
				});
				it('should  be a form', function () {
					var formController = new FormController();
					formController.isAForm(TestForm)
				});
				it('should not be a form', function () {
					var formController = new FormController()
					, res = function(){
						formController.isAForm({})
					}
					expect(res).to.throw(/this obj is not a form/);
				});
				it('should add form', function () {
					var formController = new FormController();
					formController.addForm(TestForm)
				});
				it('should return a form', function () {
					var formController = new FormController();
					formController.addForm(TestForm)
					var form = formController.getForm(TestForm.prototype.urlRoot)
					expect(form.prototype.urlRoot).to.be.deep.equal(TestForm.prototype.urlRoot)
				});
				it('reqres should return a form', function () {
					var formController = new FormController();
					formController.addForm(TestForm)
					var form = reqres.request('form', TestForm.prototype.urlRoot)
					expect(form.prototype.urlRoot).to.be.deep.equal(TestForm.prototype.urlRoot)
				});
				/*
				Form controller will receive query string arguments pertaining the forms 
				in the series. Each time a form is completed. The form controller will 
				update the querystring so that if the page is refreshed, the state can be
				regained.
				 */
			});

		});

}).call( this );